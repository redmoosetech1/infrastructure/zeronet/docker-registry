terraform {
  backend "http" {
    address = "http://mirror.rmt/terraform_state/registry.rmt"
    lock_address = "http://mirror.rmt/terraform_lock/registry.rmt"
    lock_method = "PUT"
    unlock_address = "http://mirror.rmt/terraform_lock/registry.rmt"
    unlock_method = "DELETE"
  }

required_providers {
    vsphere = {
      source = "hashicorp/vsphere"
      version = "2.0.2"
    }
  }
}

module "production" {
  source           = "git::ssh://git@gitlab.rmt:2222/infrastructure/automation/vsphere-terraform-module.git"
  vsphere_user     = var.vsphere_user
  vsphere_password = var.vsphere_password
  vsphere_server   = var.vsphere_server
  datacenter       = var.datacenter
  datastore_1      = var.datastore_1
  datastore_2      = var.datastore_2
  pool             = var.pool
  network          = var.network
  template         = var.template
  folder           = var.folder
  admin_user       = var.admin_user
  admin_password   = var.admin_password
  ssh_pub_key      = var.ssh_pub_key
  machines         = var.machines
}
