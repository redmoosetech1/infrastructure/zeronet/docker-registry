datacenter = "Datacenter"
datastore_1 = "esx0_local_storage"
datastore_2 = "Storage"
pool = "Resources"
network = "Zeronet"
template = "rocky8.5_server_amd64_template"
folder = "Infrastructure/Production/Config Management"
machines = {
 registry = {
  hostname = "registry"
  domain = "rmt"
  ipv4_address = "172.16.0.15"
  ipv4_netmask = 24
  ipv4_gateway = "172.16.0.254"
  dns_domain = "rmt"
  dns_servers = ["172.16.0.17", "172.16.0.18"]
  num_cpus = 2
  num_cores_per_socket = 2
  memory = 2048
  disks = []
 }
}
