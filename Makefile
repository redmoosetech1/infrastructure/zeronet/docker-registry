REGISTRY_IMAGE_NAME=redmoosetech/registry
REGISTRY_VERSION=production
REDIS_IMAGE_NAME=redmoosetech/redis
REDIS_VERSION=production

.PHONY: build deploy destroy
default: build

build:
	docker buildx build --platform linux/amd64,linux/arm64 -t ${REGISTRY_IMAGE_NAME}:${REGISTRY_VERSION} -t ${REGISTRY_IMAGE_NAME}:latest registry/ --push
	docker buildx build --platform linux/amd64,linux/arm64 -t ${REDIS_IMAGE_NAME}:${REDIS_VERSION} -t ${REDIS_IMAGE_NAME}:latest redis/ --push

deploy:
	docker-compose rm -sf
	docker-compose pull
	docker-compose up -d

destroy:
	docker-compose rm -sf
	docker system prune -af
